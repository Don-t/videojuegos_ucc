﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator animator;
    //  Determinamos si está avanzando o está quieto 
    private bool avanzando; 

    // Variables relacionadas con el movimiento del personaje
    public float velocidad = 3.0f;
    // Para las funciones relacionadas con físicas. Garantizar el acceso al Rigid body del personaje
    private Rigidbody rb;
    // Start is called before the first frame update
    Vector3 direccion;
    void Start()
    {
        // Iniciamos y accedemos al RigidBody
        rb = GetComponent<Rigidbody>();
        //Acceder al componente de animaciones
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    // Se ejecuta cada frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Movimiento(horizontal, vertical);
        //Animación de caminar
        Animacion(horizontal, vertical);
    }

    public void Animacion(float horizontal, float vertical){
        avanzando = horizontal != 0f || vertical !=0f;
        animator.SetBool("walk", avanzando);
    }

    public void Movimiento(float horizontal, float vertical)
    {
        direccion.Set(horizontal, 0f, vertical);
        direccion = direccion.normalized * velocidad * Time.deltaTime;
        // Validar Rigbody
        if (rb)
        {
            if (horizontal > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.right * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (horizontal < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.left * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (vertical > 0.0f)
            {
                rb.transform.Translate(
                    Vector3.forward * velocidad * Time.deltaTime
                );
            }
            if (vertical < 0.0f)
            {
                rb.transform.Translate(
                    Vector3.back * velocidad * 0.5f * Time.deltaTime
                );
            }

        }
    }
}